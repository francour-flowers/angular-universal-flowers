FROM node:10-alpine

LABEL maintainer="Alex Smolin <alexsmolin@me.com>"

WORKDIR /usr/src/app

COPY ./dist ./dist

EXPOSE 4000

# Serve the app
CMD ["node", "dist/server.js"]
