export const environment = {
  production: true,
  apiUrl: 'https://api.francourflowers.ru/v1',
  imgUrl: 'https://francourflowers.ru/images',
  iconUrl: 'https://francourflowers.ru/icons',
};
