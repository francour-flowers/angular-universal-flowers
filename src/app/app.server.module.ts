import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { LOCALSTORAGE, TINY_SLIDER } from '@app/local-storage';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';

@NgModule({
  imports: [
    AppModule,
    ServerTransferStateModule,
    ServerModule,
    ModuleMapLoaderModule,
  ],
  providers: [
    {
      provide: LOCALSTORAGE,
      useValue: {
        getItem(): string {
          return null;
        }
      }
    },
    {
      provide: TINY_SLIDER,
      useValue: {
        create(options: any): any {
          return {
            // tslint:disable-next-line:no-empty
            destroy(): void {
            },
            // tslint:disable-next-line:no-empty
            goTo(number: void): void {
            },
            events: {
              // tslint:disable-next-line:no-empty
              on(event: any, cb: (info: any) => any): void {
              },
            }
          };
        }
      }
    }
  ],
  bootstrap: [AppComponent],
})
export class AppServerModule {
}
