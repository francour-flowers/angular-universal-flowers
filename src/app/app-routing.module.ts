import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartComponent } from '@app/modules/core/components/cart/cart.component';
import { FavoritesComponent } from '@app/modules/core/components/favorites/favorites.component';
import { CartResolver } from '@app/modules/core/resolvers/cart.resolver';
import { FavoritesResolver } from '@app/modules/core/resolvers/favorites.resolver';
import { GlobalDataResolver } from '@app/modules/core/resolvers/global-data.resolver';
import { MainLayoutComponent } from './modules/core/components/main-layout/main-layout.component';
import { PageNotFoundComponent } from './modules/shared/components/page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: '',
    data: {breadcrumb: 'Главная'},
    resolve: {
      mainData: GlobalDataResolver,
    },
    children: [
      {
        path: '',
        component: MainLayoutComponent,
        children: [
          {
            path: '',
            loadChildren: './modules/home/home.module#HomeModule',
          },
          {
            path: 'favorites',
            runGuardsAndResolvers: 'always',
            component: FavoritesComponent,
            resolve: {
              favorites: FavoritesResolver
            }
          },
          {
            path: 'cart',
            runGuardsAndResolvers: 'always',
            component: CartComponent,
            resolve: {
              cart: CartResolver
            }
          },
          {
            path: 'product',
            loadChildren: './modules/product/product.module#ProductModule',
          },
          {
            path: 'catalog',
            loadChildren: './modules/catalog/catalog.module#CatalogModule',
          },
          {
            path: ':slug',
            loadChildren: './modules/page/page.module#PageModule',
          },
        ],
      },
      {path: '**', component: PageNotFoundComponent},
    ],
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled',
      scrollPositionRestoration: 'enabled',
      urlUpdateStrategy: 'eager',
      onSameUrlNavigation: 'reload'
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
