import { InjectionToken } from '@angular/core';
import { Config } from 'protractor';

export const LOCALSTORAGE = new InjectionToken<Config>('LocalStorage');
export const TINY_SLIDER = new InjectionToken<Config>('TinySlider');
