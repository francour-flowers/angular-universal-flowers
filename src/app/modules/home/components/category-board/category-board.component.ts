import { Category, Product, ProductField, ProductService, QueryParams } from '@alexsmolin/shop-core';
import { Component, Input, OnInit } from '@angular/core';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-category-board',
  templateUrl: './category-board.component.html',
  styleUrls: ['./category-board.component.scss']
})
export class CategoryBoardComponent implements OnInit {
  products: Array<Product> = [];
  countShow: number = 0;
  totalCount: number = 0;
  /**
   * Категория
   */
  @Input() category: Category;

  constructor(private productService: ProductService) {
  }

  ngOnInit(): void {
    this.moreShow();
  }

  /**
   * Подгрузить еще
   */
  moreShow(): void {
    const queryParams: QueryParams<ProductField> = {
      limit: 4,
      offset: this.countShow,
      filter: [
        {
          field: ProductField.CATEGORY_ID,
          value: this.category.id.toString(),
        }
      ]
    };

    this.productService.getList(queryParams)
      .pipe(
        tap(products => {
          this.products = [...this.products, ...products.data];
          this.countShow = this.countShow + 4;
          this.totalCount = products.totalCount;
        }),
      )
      .subscribe();
  }

}
