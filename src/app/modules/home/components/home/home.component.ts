import { Category, MenuType, ProductService } from '@alexsmolin/shop-core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MainData } from '@app/modules/core/interfaces/main-data';
import { Slide } from '@app/modules/shared/components/slider/slider.component';
import { Observable } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  categories: Observable<Array<Category>>;
  /**
   * Слайды
   */
  slides: Observable<Array<Slide>>;

  constructor(private route: ActivatedRoute, private productService: ProductService) {
  }

  ngOnInit(): void {
    this.categories = this.route.data
      .pipe(
        map(data => data['mainData']),
        filter(Boolean),
        map((mainData: MainData) => mainData.categories),
        shareReplay(1),
      );

    this.slides = this.categories
      .pipe(
        map(categories => categories
          .filter(category => Boolean(category.image))
          .map(category => ({
            image: category.image,
            links: [
              {
                type: MenuType.Category,
                category: category
              }
            ]
          }) as Slide)),
        shareReplay(1),
      );

  }

}
