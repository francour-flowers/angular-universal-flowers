import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/modules/shared/shared.module';
import { CategoryBoardComponent } from './components/category-board/category-board.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full',
      }
    ])
  ],
  providers: [],
  declarations: [HomeComponent, CategoryBoardComponent],
})
export class HomeModule {
}
