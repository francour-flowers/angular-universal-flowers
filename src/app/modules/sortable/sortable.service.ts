import { Injectable } from '@angular/core';

@Injectable()
export class SortableService {
  page: number;
  lastPage: number;

  getPagination(): Array<number> {
    const delta: number = 2;
    const left: number = this.page - delta;
    const right: number = this.page + delta + 1;
    const range: Array<number> = [];
    const rangeWithDots: Array<number> = [];
    let l: number = null;

    if (this.page <= this.lastPage && this.lastPage > 1) {
      for (let i = 1; i <= this.lastPage; i++) {
        if (i === 1 || i === this.lastPage || i >= left && i < right) {
          range.push(i);
        }
      }

      range.forEach(page => {
        if (l) {
          if (page - l === 2) {
            rangeWithDots.push(l + 1);
          } else if (page - l !== 1) {
            rangeWithDots.push(null);
          }
        }
        rangeWithDots.push(page);
        l = page;
      });
    }

    return rangeWithDots;
  }
}
