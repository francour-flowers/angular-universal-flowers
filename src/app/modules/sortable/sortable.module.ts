import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/modules/shared/shared.module';
import { SortableComponent } from '@app/modules/sortable/sortable.component';
import { SortableService } from '@app/modules/sortable/sortable.service';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule,
    MatIconModule,
    SharedModule
  ],
  declarations: [
    SortableComponent,
  ],
  exports: [
    SortableComponent
  ],
})
export class SortableModule {
  static forChild(): ModuleWithProviders {
    return {
      ngModule: SortableModule,
      providers: [
        SortableService,
      ]
    };
  }
}
