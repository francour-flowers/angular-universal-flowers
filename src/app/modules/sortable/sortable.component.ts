import { ForOrder, ProductField } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface SortableOption {
  title: string;
  sort?: ForOrder;
  field?: string;
}

@Component({
  selector: 'app-sortable',
  templateUrl: './sortable.component.html',
  styleUrls: ['./sortable.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SortableComponent implements OnInit {
  queryParams: Observable<Params>;

  options: Array<SortableOption> = [
    {
      title: 'по новизне',
    },
    {
      title: 'цена по возрастанию',
      sort: ForOrder.ASC,
      field: 'price',
    },
    {
      title: 'цена по убыванию',
      field: 'price',
      sort: ForOrder.DESC
    }
  ];

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.queryParams = this.route.queryParams;
  }

  isActive(params: Params, field: string, sort: ForOrder): boolean {
    const order = params.sort && params.sort.charAt(0) === '-' ? ForOrder.DESC : ForOrder.ASC;
    const field2 = params.sort && order === ForOrder.DESC ? params.sort.substr(1) : params.sort;
    if (!(<any>Object).values(ProductField).includes(field2)) {
      return !sort;
    }

    return params.sort === (sort === ForOrder.DESC ? `-${field}` : field);
  }

  getActiveOption(): Observable<SortableOption> {
    return this.queryParams
      .pipe(
        map(queryParams => {
          return this.options.find(opt => this.isActive(queryParams, opt.field, opt.sort));
        }),
      );
  }

  getQueryParams(params: Params, field: string, sort: ForOrder): Params {
    params = {...params};

    if (!this.isActive(params, field, sort)) {
      delete params['page'];
    }
    return {...params, sort: sort === ForOrder.DESC ? `-${field}` : field};
  }

  setSort(queryParams: Params, option: SortableOption): void {
    this.router.navigate([], {queryParams: this.getQueryParams(queryParams, option.field, option.sort)});
  }
}
