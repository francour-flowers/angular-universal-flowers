import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PageResolver } from '@app/modules/page/resolvers/page.resolver';
import { SharedModule } from '../shared/shared.module';
import { PageComponent } from './components/page/page.component';

@NgModule({
  declarations: [PageComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PageComponent,
        pathMatch: 'full',
        resolve: {
          page: PageResolver,
        },
      }
    ]),
  ],
  providers: [
    PageResolver,
  ],
})
export class PageModule {
}
