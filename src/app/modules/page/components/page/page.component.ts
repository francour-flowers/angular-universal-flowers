import { Page, ProductService } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CanonicalService } from '@app/modules/core/services/canonical.service';
import { MetaService } from '@ngx-meta/core';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [
    CanonicalService,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageComponent implements OnInit, OnDestroy {
  /**
   * Страница
   */
  page: Observable<Page>;

  constructor(private productService: ProductService,
              private metaService: MetaService,
              private canonicalService: CanonicalService,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.page = this.route.data
      .pipe(
        map(data => data['page']),
        tap((p: Page) => {
          if (p) {
            if (p.metaTags) {
              this.metaService.setTitle(p.metaTags.title || p.name, true);
              this.metaService.setTag('description', p.metaTags.description);
              this.metaService.removeTag('description');
              this.metaService.setTag('keywords', p.metaTags.keywords);
            } else {
              this.metaService.setTitle(p.name, true);
            }
            this.canonicalService.create();
          } else {
            this.metaService.setTitle('Такой страницы не существует', true);
            this.canonicalService.remove();
          }
        }),
        shareReplay(1),
      );
  }

  ngOnDestroy(): void {
    this.canonicalService.remove();
    this.metaService.removeTag('title');
    this.metaService.removeTag('description');
    this.metaService.removeTag('keywords');
  }

}
