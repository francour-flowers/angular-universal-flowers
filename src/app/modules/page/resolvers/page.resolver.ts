import { Page, PageService } from '@alexsmolin/shop-core';
import { HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class PageResolver implements Resolve<Page> {
  private readonly response: Response;

  constructor(private pageService: PageService,
              private router: Router,
              @Optional() @Inject(RESPONSE) response: any) {
    this.response = response;
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Page> {
    const slug = route.paramMap.get('slug');

    return this.pageService.getBySlug(slug)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.setNotFound();
          }
          return EMPTY;
        })
      );
  }

  private setNotFound(message: string = 'not found'): void {
    if (this.response) {
      this.response.statusCode = 404;
      this.response.statusMessage = message;
    }
  }
}
