import { Inject, Injectable } from '@angular/core';
import { LOCALSTORAGE } from '@app/local-storage';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class GridSelectorService {

  private readonly isMore: BehaviorSubject<boolean>;

  constructor(@Inject(LOCALSTORAGE) private localStorage: any) {
    this.isMore = new BehaviorSubject(this.getIsMoreOfStorage());
  }

  /**
   * Переключить
   */
  setIsMore(isMore: boolean): void {
    if (this.isMore.value !== isMore) {
      this.localStorage.setItem('grid-selector-is-more', isMore ? '1' : '');
      this.isMore.next(isMore);
    }
  }

  /**
   * Получить значение
   */
  getIsMore(): Observable<boolean> {
    return this.isMore.asObservable();
  }

  /**
   * Получить значение из хранилища
   */
  private getIsMoreOfStorage(): boolean {
    const isMore = this.localStorage.getItem('grid-selector-is-more');
    return !!isMore;
  }
}
