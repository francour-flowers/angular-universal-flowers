import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { GridSelectorComponent } from '@app/modules/grid-selector/grid-selector.component';
import { GridSelectorService } from '@app/modules/grid-selector/grid-selector.service';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatButtonToggleModule,
    FormsModule,
  ],
  declarations: [
    GridSelectorComponent,
  ],
  exports: [
    GridSelectorComponent
  ],
})
export class GridSelectorModule {
  static forChild(): ModuleWithProviders {
    return {
      ngModule: GridSelectorModule,
      providers: [
        GridSelectorService,
      ]
    };
  }
}
