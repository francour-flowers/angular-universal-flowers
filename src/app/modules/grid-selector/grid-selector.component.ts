import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { GridSelectorService } from '@app/modules/grid-selector/grid-selector.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-grid-selector',
  templateUrl: './grid-selector.component.html',
  styleUrls: ['./grid-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GridSelectorComponent implements OnInit {
  isMore: boolean = true;

  constructor(private gridSelectorService: GridSelectorService) {
  }

  ngOnInit(): void {
    this.gridSelectorService.getIsMore()
      .pipe(
        tap(isMore => this.isMore = isMore),
      )
      .subscribe();
  }

  changeIsMore(isMore: boolean): void {
    this.gridSelectorService.setIsMore(isMore);
  }
}
