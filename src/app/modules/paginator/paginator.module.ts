import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { PaginatorComponent } from '@app/modules/paginator/paginator.component';
import { PaginatorService } from '@app/modules/paginator/paginator.service';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    RouterModule,
    MatIconModule
  ],
  declarations: [
    PaginatorComponent,
  ],
  exports: [
    PaginatorComponent
  ],
})
export class PaginatorModule {
  static forChild(): ModuleWithProviders {
    return {
      ngModule: PaginatorModule,
      providers: [
        PaginatorService,
      ]
    };
  }
}
