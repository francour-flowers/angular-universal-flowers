import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PaginatorData, PaginatorService } from '@app/modules/paginator/paginator.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginatorComponent implements OnInit {
  queryParams: Observable<Params>;
  paginatorData: Observable<PaginatorData>;

  constructor(private route: ActivatedRoute,
              private paginatorService: PaginatorService) {
  }

  ngOnInit(): void {
    this.queryParams = this.route.queryParams;
    this.paginatorData = this.paginatorService.getPaginationData();
  }

  getQueryParams(params: Params, page: number): Params {
    params = {...params};
    delete params['page'];
    return {page: page === 1 ? null : page, ...params};
  }
}
