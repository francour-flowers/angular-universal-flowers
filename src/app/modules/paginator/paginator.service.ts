import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Параметры пагинатора
 */
export interface PaginatorData {
  /**
   * Текущая страница
   */
  currentPage: number;
  /**
   * Кол-во страниц
   */
  countPage: number;
  /**
   * Страницы
   */
  pages: Array<number | null>;
}

@Injectable()
export class PaginatorService {
  private paginatorDataBehaviorSubject: BehaviorSubject<PaginatorData> = new BehaviorSubject<PaginatorData>(null);

  /**
   * Обновить состояние пагинатора
   * @param currentPage
   * @param countPage
   */
  updatePaginator(currentPage: number = 1, countPage: number = 1): void {
    const delta: number = 2;
    const left: number = currentPage - delta;
    const right: number = currentPage + delta + 1;
    const range: Array<number> = [];
    const rangeWithDots: Array<number> = [];
    let l: number = null;

    if (currentPage <= countPage && countPage > 1) {
      for (let i = 1; i <= countPage; i++) {
        if (i === 1 || i === countPage || i >= left && i < right) {
          range.push(i);
        }
      }

      range.forEach(page => {
        if (l) {
          if (page - l === 2) {
            rangeWithDots.push(l + 1);
          } else if (page - l !== 1) {
            rangeWithDots.push(null);
          }
        }
        rangeWithDots.push(page);
        l = page;
      });
    }

    this.paginatorDataBehaviorSubject.next({
      currentPage: currentPage,
      countPage: countPage,
      pages: rangeWithDots,
    });
  }

  /**
   * Получить данные пагинатора
   */
  getPaginationData(): Observable<PaginatorData> {
    return this.paginatorDataBehaviorSubject.asObservable();
  }
}
