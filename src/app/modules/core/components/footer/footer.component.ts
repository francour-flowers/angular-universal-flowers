import { Menu } from '@alexsmolin/shop-core';
import { Component, Input } from '@angular/core';
import { MainData } from '../../interfaces/main-data';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  @Input() footerMenu: Menu;
  @Input() footerMenuMobile: Menu;
  @Input() socialMenu: Menu;
  @Input() mainData: MainData;

  /**
   * Получить текущий год
   */
  getCurrentYear(): number {
    return new Date().getFullYear();
  }
}
