import { Image, Order, OrderProduct, OrderService, Product } from '@alexsmolin/shop-core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CartProduct, CartService } from '@app/modules/core/services/cart.service';
import { Breadcrumb } from '@app/modules/shared/components/breadcrumbs/breadcrumbs.component';
import { OrderDialogComponent } from '@app/modules/shared/components/order-dialog/order-dialog.component';
import { MetaService } from '@ngx-meta/core';
import { of } from 'rxjs';
import { defaultIfEmpty, filter, first, flatMap, map, switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  /**
   * Заголовок
   */
  title: string = 'Оформление заказа';
  /**
   * Хлебные крошки
   */
  breadcrumbs: Array<Breadcrumb> = [{name: this.title}];
  /**
   * Список продуктов
   */
  productList: Array<{ product: Product, cartProduct: CartProduct }> = [];
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private dialog: MatDialog,
              private metaService: MetaService,
              private cartService: CartService,
              private orderService: OrderService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.metaService.setTitle(this.title, true);

    this.route.data
      .pipe(
        map(data => data['cart']),
        flatMap(cart => of(cart)
          .pipe(
            filter(Boolean),
            switchMap(productList => this.cartService.getList()
              .pipe(
                first(),
                map(cartProducts => {
                  return productList.data.map(product => ({
                    product: product,
                    cartProduct: cartProducts.find(cartProduct => cartProduct.productId === product.id),
                  }));
                }),
              )),
            defaultIfEmpty([])
          )),
        tap(productList => {
          this.productList = productList;
        }),
      )
      .subscribe();

    this.firstFormGroup = this.fb.group({
      firstName: ['', Validators.required],
      phone: ['', Validators.required],
    });
    // this.secondFormGroup = this.fb.group({
    //   secondCtrl: ['', Validators.required]
    // });
  }

  /**
   * Заказать
   */
  order(): void {
    const formData = this.firstFormGroup.getRawValue();
    const order: Order = {
      name: formData['firstName'],
      note: formData['note'],
      orderProducts: this.productList.map(pc => {
        return {
          count: pc.cartProduct.count,
          productId: pc.product.id,
        } as OrderProduct;
      }),
      phone: formData['phone'],

    } as Order;
    this.orderService.create(order)
      .pipe(
        tap(saveOrder => {
          this.cartService.clear();
          this.productList = [];
          this.dialog.open(OrderDialogComponent, {
            data: saveOrder,
          });
          console.log(saveOrder);
        }),
      )
      .subscribe();
  }

  getMainImage(product: Product): Image {
    const productImage = product.productImages.find(productImg => productImg.main);
    if (productImage) {
      return productImage.image;
    }
    return null;
  }

  delete(id: number): void {
    this.cartService.removeProduct(id);
    this.productList = this.productList.filter(productCart => productCart.product.id !== id);
  }

  trackByFn(index: number, item: any): number {
    return index; // or item.id
  }

  /**
   * Получить общую цену в корзине
   */
  getTotalPrice(): number {
    return this.productList.map(productCart => productCart.product.price * productCart.cartProduct.count)
      .reduce((price, totalPrice) => price + totalPrice, 0);
  }

  /**
   * Перезагрузить страницу
   */
  private reloadPage(): void {
    this.router.navigate([this.router.url]);
  }
}
