import { Menu } from '@alexsmolin/shop-core';
import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, NgZone, OnDestroy } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, filter, takeUntil, tap } from 'rxjs/operators';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements AfterViewInit, OnDestroy {
  /**
   * Статус меню
   */
  idOpenMenu: number = null;
  /**
   * Основное меню
   */
  @Input() menu: Menu;
  @Input() menuMobile: Menu;
  /**
   * Для отписки
   */
  private ngUnsubscribe: Subject<void> = new Subject();

  constructor(private route: ActivatedRoute,
              private router: Router,
              private _zone: NgZone,
              private element: ElementRef,
              private cd: ChangeDetectorRef) {
  }

  ngAfterViewInit(): void {
    this._zone.runOutsideAngular(() => {
      const listMenu = this.element.nativeElement.querySelectorAll('.header-menu > li > a:not([data-menu-length-links=\'0\'])');
      const megaMenuContainer = this.element.nativeElement.querySelector('.mega-menu-container');

      let blockClose = false;

      listMenu.forEach(menu => {
        const idMenu = menu.getAttribute('data-menu-id');

        if (idMenu && !isNaN(Number(idMenu))) {
          fromEvent(menu, 'mouseenter')
            .pipe(
              tap(() => blockClose = true),
              debounceTime(100),
              filter(() => blockClose),
              tap(() => {
                this.openGlobalMenu(Number(idMenu));
                this.cd.detectChanges();
              }),
              takeUntil(this.ngUnsubscribe)
            )
            .subscribe();

          fromEvent(menu, 'mouseleave')
            .pipe(
              tap(() => blockClose = false),
              debounceTime(100),
              filter(() => !blockClose),
              tap(() => {
                if (Number(idMenu) === this.idOpenMenu) {
                  this.closeGlobalMenu();
                  this.cd.detectChanges();
                }
              }),
              takeUntil(this.ngUnsubscribe)
            )
            .subscribe();
        }
      });

      if (megaMenuContainer) {
        fromEvent(megaMenuContainer, 'mouseenter')
          .pipe(
            tap(() => blockClose = true),
            debounceTime(100),
            filter(() => blockClose),
            takeUntil(this.ngUnsubscribe),
          )
          .subscribe();

        fromEvent(megaMenuContainer, 'mouseleave')
          .pipe(
            tap(() => blockClose = false),
            debounceTime(100),
            filter(() => !blockClose),
            tap(() => {
              this.closeGlobalMenu();
              this.cd.detectChanges();
            }),
            takeUntil(this.ngUnsubscribe)
          )
          .subscribe();
      }
    });

    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.closeGlobalMenu();
      }
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  openGlobalMenu(id: number): void {
    this.closeGlobalMenu();

    this.idOpenMenu = id;
  }

  closeGlobalMenu(): void {
    this.idOpenMenu = null;
  }

}
