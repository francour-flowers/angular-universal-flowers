import { Category, ForOrder, Product, ProductField, ProductService, QueryParams } from '@alexsmolin/shop-core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { first, groupBy, map, mergeMap, switchMap, toArray } from 'rxjs/operators';

export interface StateGroup {
  category: Category;
  products: Array<Product>;
}

export const _filter = (opt: string[], value: string): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  stateForm: FormGroup = this.fb.group({
    stateGroup: '',
  });

  stateGroupOptions: Observable<StateGroup[]>;

  constructor(private router: Router,
              private fb: FormBuilder,
              private productService: ProductService) {
  }

  ngOnInit(): void {
    this.stateGroupOptions = this.stateForm.get('stateGroup')!.valueChanges
      .pipe(
        switchMap(textSearch => {
          const queryParams: QueryParams<ProductField> = {
            limit: 15,
            sort: [
              {
                field: ProductField.CREATED_AT,
                order: ForOrder.DESC
              }
            ],
            filter: [
              {
                field: ProductField.NAME,
                value: textSearch
              }
            ],
          };

          return this.productService.getList(queryParams)
            .pipe(
              first(),
              map(listProduct => listProduct.data),
            );
        }),
        mergeMap(products => from(products)
          .pipe(
            groupBy(product => {
              const productCategory = product.productCategories.find(pc => pc.main);
              if (productCategory) {
                return productCategory.categoryId;
              }
              return null;
            }),
            mergeMap(group => {
              return group.pipe(toArray());
            }),
            map(productsInGroup => {
              const productCategory = productsInGroup[0].productCategories.find(pg => pg.main);
              return {
                category: productCategory ? productCategory.category : null,
                products: productsInGroup,
              };
            }),
            toArray(),
          )),
      );
  }

  clear(): void {
    this.stateForm.get('stateGroup').setValue('');
  }

  displayFn(product?: Product): string | undefined {
    return product ? product.name : undefined;
  }

  viewProduct(product: Product): void {
    this.clear();
    this.router.navigate(['/', 'product', product.slug]);
  }
}
