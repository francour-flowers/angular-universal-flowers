import { Menu } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MainData } from '@app/modules/core/interfaces/main-data';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
  isOpenSearchMobile: boolean = false;
  /**
   * Верхнее меню
   */
  @Input() topMenu: Menu;
  @Input() mainData: MainData;
  @Input() countFavorites: number = 0;
  @Input() countCartProducts: number = 0;
  /**
   * Открытие меню
   */
  @Output() clickMenu: EventEmitter<void> = new EventEmitter();

  getCountFavorites(): string {
    return this.countFavorites ? String(this.countFavorites) : null;
  }

  getCountCartProducts(): string {
    return this.countCartProducts ? String(this.countCartProducts) : null;
  }
}
