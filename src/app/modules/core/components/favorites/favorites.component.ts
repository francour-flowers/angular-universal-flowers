import { List, Product } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Breadcrumb } from '@app/modules/shared/components/breadcrumbs/breadcrumbs.component';
import { MetaService } from '@ngx-meta/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoritesComponent implements OnInit {
  /**
   * Заголовок
   */
  title: string = 'Избранное';
  /**
   * Хлебные крошки
   */
  breadcrumbs: Array<Breadcrumb> = [{name: 'Отложенное'}];
  /**
   * Список продуктов
   */
  productList: Observable<List<Product>>;

  constructor(private route: ActivatedRoute,
              private metaService: MetaService) {
  }

  ngOnInit(): void {
    this.metaService.setTitle('Избранное', true);

    this.productList = this.route.data
      .pipe(
        map(data => data['favorites']),
        shareReplay(1),
      );
  }

}
