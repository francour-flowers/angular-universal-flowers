import { Menu } from '@alexsmolin/shop-core';
import { Component, DoCheck, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { CartService } from '@app/modules/core/services/cart.service';
import { ProductLikeService } from '@app/modules/core/services/product-like.service';
import { Observable } from 'rxjs';
import { defaultIfEmpty, filter, map, shareReplay } from 'rxjs/operators';
import { MainData } from '../../interfaces/main-data';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent implements OnInit, DoCheck {

  static doCheckCounter: number = 0;

  checkPosition: number;

  mainData: Observable<MainData>;
  headerTopMenu: Observable<Menu>;
  /**
   * Основное меню
   */
  mainMenu: Observable<Menu>;
  mainMenuMobile: Observable<Menu>;
  footerMenu: Observable<Menu>;
  footerMenuMobile: Observable<Menu>;
  socialMenu: Observable<Menu>;
  sidebarMenu: Observable<Menu>;

  countFavorites: Observable<number>;
  countCartProducts: Observable<number>;

  @ViewChild('drawer') drawer: MatDrawer;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private productLikeService: ProductLikeService,
              private cartService: CartService) {
  }

  ngDoCheck(): void {
    this.checkPosition = MainLayoutComponent.doCheckCounter;
    MainLayoutComponent.doCheckCounter += 1;
  }

  ngOnInit(): void {

    this.router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.drawer.close();
      }
    });

    this.mainData = this.route.data
      .pipe(
        map(data => data['mainData']),
        filter(Boolean),
        shareReplay(1),
      );

    this.headerTopMenu = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'top')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.mainMenu = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'main')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.mainMenuMobile = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'main-mobile')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.footerMenu = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'footer')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.footerMenuMobile = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'footer-mobile')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.socialMenu = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'social')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.sidebarMenu = this.mainData
      .pipe(
        map(md => md.menus.find(menu => menu.slug === 'sidebar')),
        defaultIfEmpty(null),
        shareReplay(1),
      );

    this.countFavorites = this.productLikeService.getList()
      .pipe(
        map(ids => ids.length),
        shareReplay(1),
      )
      .pipe();

    this.countCartProducts = this.cartService.getList()
      .pipe(
        map(cartProducts => cartProducts.length),
        shareReplay(1),
      )
      .pipe();
  }

  getCountFavorites(): Observable<string> {
    return this.countFavorites
      .pipe(
        map(countFavorites => countFavorites ? String(countFavorites) : null),
        shareReplay(1),
      );
  }

  getCountCartProducts(): Observable<string> {
    return this.countCartProducts
      .pipe(
        map(countCartProducts => countCartProducts ? String(countCartProducts) : null),
        shareReplay(1),
      );
  }

}
