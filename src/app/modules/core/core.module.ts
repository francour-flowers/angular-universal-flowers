import { ShopCoreModule } from '@alexsmolin/shop-core';
import { HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GridSelectorModule } from '@app/modules/grid-selector/grid-selector.module';
import { PaginatorModule } from '@app/modules/paginator/paginator.module';
import { SortableModule } from '@app/modules/sortable/sortable.module';
import { SortableService } from '@app/modules/sortable/sortable.service';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { SharedModule } from '../shared/shared.module';
import { CartComponent } from './components/cart/cart.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { MainLayoutComponent } from './components/main-layout/main-layout.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { SearchComponent } from './components/search/search.component';
import { MainService } from './services/main.service';

@NgModule({
  imports: [
    TransferHttpCacheModule,
    HttpClientModule,
    RouterModule,
    SharedModule,
    ShopCoreModule,
    PaginatorModule.forChild(),
    SortableModule.forChild(),
    GridSelectorModule.forChild(),
  ],
  declarations: [
    MainLayoutComponent,
    HeaderComponent,
    SearchComponent,
    FooterComponent,
    FavoritesComponent,
    CartComponent,
    MainMenuComponent,
  ],
  providers: [
    MainService,
  ],
  exports: [
    HttpClientModule,
    PaginatorModule,
    SortableModule,
    MainLayoutComponent,
    FooterComponent,
    FavoritesComponent,
    CartComponent,
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        SortableService,
      ]
    };
  }
}
