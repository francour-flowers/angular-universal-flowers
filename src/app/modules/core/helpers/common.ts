import { Category, MenuLink, MenuType } from '@alexsmolin/shop-core';

/**
 * Возвращает категории до последнего родительского
 * @param parentCategoryId
 * @param categories
 */
export function getParentsCategories(parentCategoryId: number, categories: Array<Category>): Array<Category> {
  let parentsCategories = [];

  const parentCategory = categories.find(c => c.id === parentCategoryId);

  if (parentCategory) {
    parentsCategories.push(parentCategory);

    if (parentCategory.parentId) {
      parentsCategories = [].concat(getParentsCategories(parentCategory.parentId, categories), parentsCategories);
    }
  }

  return parentsCategories;
}

/**
 * Возвращает категории водном массиве
 * @param categories
 */
export function getFlatCategories(categories: Array<Category>): Array<Category> {
  let flatCategories = [...categories];
  categories.forEach(c => {
    if (c.children && c.children.length) {
      flatCategories = [...flatCategories, ...this.getFlatCategories(c.children)];
    }
  });

  return flatCategories;
}

/**
 * Функия для поиска имени в элементе меню
 * @param link
 */
export function getNameLink(link: MenuLink): string {
  switch (link.type) {
    case MenuType.Page:
      return link.page.name;
    case MenuType.Category:
      return link.category.name;
    case MenuType.Product:
      return link.product.name;
    case MenuType.Gag:
    case MenuType.Blog:
    case MenuType.CustomLink:
    default:
      return link.name;
  }
}
