import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { MainData } from '@app/modules/core/interfaces/main-data';
import { Observable, of } from 'rxjs';
import { catchError, first } from 'rxjs/operators';
import { MainService } from '../services/main.service';

/**
 * Резолвер для загрузки основных данных на сайт
 */
@Injectable({
  providedIn: 'root'
})
export class GlobalDataResolver implements Resolve<any> {
  constructor(private mainService: MainService) {
  }

  resolve(): Observable<MainData> {
    return this.mainService.getMainData()
      .pipe(
        first(),
        catchError((err) => {
          return of(null);
        }),
      );
  }

}
