import { ForOrder, List, Product, ProductField, ProductService, QueryParams, Sort } from '@alexsmolin/shop-core';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ProductLikeService } from '@app/modules/core/services/product-like.service';
import { PaginatorService } from '@app/modules/paginator/paginator.service';
import { Observable } from 'rxjs';
import { defaultIfEmpty, filter, first, map, switchMap, tap } from 'rxjs/operators';

/**
 * Сортировка по умолчанию
 */
const DEFAULT_SORT = {
  field: ProductField.CREATED_AT,
  order: ForOrder.DESC
};

const LIMIT = 24;

/**
 * Резолвер для загрузки понравившихся товаров
 */
@Injectable({
  providedIn: 'root'
})
export class FavoritesResolver implements Resolve<List<Product>> {

  constructor(private productService: ProductService,
              private productLikeService: ProductLikeService,
              private paginatorService: PaginatorService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<List<Product>> {
    let page = Number(route.queryParams['page']);
    page = page > 0 ? page : 1;

    return this.productLikeService.getList()
      .pipe(
        first(),
        filter(ids => !!ids.length),
        map(ids => ({
          offset: (page - 1) * LIMIT,
          limit: LIMIT,
          sort: [
            this.convertSortStrToSort(route.queryParams['sort'])
          ],
          filter: [
            {
              field: ProductField.ID,
              values: ids.map(id => String(id)),
            }
          ]
        })),
        switchMap((queryParams: QueryParams<ProductField>) => this.productService.getList(queryParams)),
        tap(list => {
          this.paginatorService.updatePaginator(page, Math.ceil(list.totalCount / LIMIT));
        }),
        defaultIfEmpty(null),
      );
  }

  private convertSortStrToSort(sort: string): Sort<ProductField> {
    const order = sort && sort.charAt(0) === '-' ? ForOrder.DESC : ForOrder.ASC;
    const field = sort && order === ForOrder.DESC ? sort.substr(1) : sort;
    if ((<any>Object).values(ProductField).includes(field)) {
      return {
        order: order,
        field: field as ProductField,
      };
    }

    return DEFAULT_SORT;
  }
}
