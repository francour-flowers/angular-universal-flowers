import { List, Product, ProductField, ProductService, QueryParams } from '@alexsmolin/shop-core';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { CartService } from '@app/modules/core/services/cart.service';
import { Observable } from 'rxjs';
import { defaultIfEmpty, filter, first, flatMap, map } from 'rxjs/operators';

/**
 * Резолвер для загрузки товаров в корзине
 */
@Injectable({
  providedIn: 'root'
})
export class CartResolver implements Resolve<List<Product>> {

  constructor(private productService: ProductService,
              private cartService: CartService) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<List<Product>> {
    return this.cartService.getList()
      .pipe(
        first(),
        filter(cartProducts => !!cartProducts.length),
        map(cartProducts => ({
          filter: [
            {
              field: ProductField.ID,
              values: cartProducts.map(cartProduct => String(cartProduct.productId)),
            }
          ]
        })),
        flatMap((queryParams: QueryParams<ProductField>) => this.productService.getList(queryParams)),
        defaultIfEmpty(null),
      );
  }
}
