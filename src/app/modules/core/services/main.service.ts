import { CategoryService, MenuService } from '@alexsmolin/shop-core';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { first, flatMap, map } from 'rxjs/operators';
import { MainData } from '../interfaces/main-data';
import { MenuType } from '../interfaces/menu';

const MAIN_DATA: MainData = {
  siteName: 'Francour Flowers',
  phoneNumber: {
    id: 1,
    name: '+7 999 999 99 99',
    type: MenuType.CustomLink,
    url: '#',
    position: 0,
    children: [],
  },
  menus: [],
  categories: [],
};

/**
 * Основной сервис
 */
@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private categoryService: CategoryService,
              private menuService: MenuService) {
  }

  /**
   * Получение основная информация
   */
  getMainData(): Observable<MainData> {
    return of(MAIN_DATA)
      .pipe(
        flatMap(mainData => this.categoryService.getList()
          .pipe(
            first(),
            map(categories => {
              mainData.categories = categories;
              return mainData;
            }),
          )),
        flatMap(mainData => this.menuService.getList()
          .pipe(
            first(),
            map(menus => {
              mainData.menus = menus;
              return mainData;
            }),
          ))
      );
  }
}
