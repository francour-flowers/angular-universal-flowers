import { Inject, Injectable } from '@angular/core';
import { LOCALSTORAGE } from '@app/local-storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductLikeService {

  private readonly changeLike: BehaviorSubject<Array<number>>;

  constructor(@Inject(LOCALSTORAGE) private localStorage: any) {
    this.changeLike = new BehaviorSubject(this.getProductsLike());
  }

  /**
   * Поменять статус понравилось
   * @param id
   */
  toggleLike(id: number): Observable<any> {
    return new Observable((observer) => {
      const productLikes = this.getProductsLike();
      const index = productLikes.indexOf(id);
      if (index > -1) {
        productLikes.splice(index, 1);
      } else {
        productLikes.push(id);
      }

      this.localStorage.setItem('product-id-likes', JSON.stringify(productLikes));

      observer.next(index === -1);
      observer.complete();

      this.changeLike.next(productLikes);
    });
  }

  /**
   * Подписка на изменения в понравившихся товара по идентификатору
   * @param id
   */
  isLike(id: number): Observable<boolean> {
    return this.changeLike.asObservable()
      .pipe(
        map(productLikes => productLikes.indexOf(id) > -1)
      );
  }

  /**
   * Получить список идентификаторов понравившихся продуктов
   */
  getList(): Observable<Array<number>> {
    return this.changeLike.asObservable();
  }

  /**
   * Получить массив идентификаторов продуктов которые понравились
   */
  private getProductsLike(): Array<number> {
    const productLikesString = this.localStorage.getItem('product-id-likes');
    let productLikes = [];
    if (productLikesString) {
      productLikes = JSON.parse(productLikesString);
    }

    return productLikes;
  }
}
