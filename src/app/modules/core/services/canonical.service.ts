import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

@Injectable()
export class CanonicalService {
  link: HTMLLinkElement;

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  create(): void {
    if (this.link) {
      this.link.setAttribute('href', this.document.URL.split('?')[0]);
    } else {
      this.link = this.document.createElement('link');
      this.link.setAttribute('rel', 'canonical');
      this.link.setAttribute('href', this.document.URL.split('?')[0]);
      this.document.head.appendChild(this.link);
    }
  }

  remove(): void {
    if (this.link) {
      this.document.head.removeChild(this.link);
    }
  }
}
