import { Inject, Injectable } from '@angular/core';
import { LOCALSTORAGE } from '@app/local-storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

/**
 * Продукт в корзине
 */
export interface CartProduct {
  /**
   * Идентификатор товара
   */
  productId: number;
  /**
   * Колличество товара
   */
  count: number;
}

/**
 * Корзина товаров
 */
@Injectable({
  providedIn: 'root'
})
export class CartService {
  /**
   * Список товаров в корзине
   */
  private readonly cartProducts: BehaviorSubject<Array<CartProduct>>;

  constructor(@Inject(LOCALSTORAGE) private localStorage: any) {
    this.cartProducts = new BehaviorSubject(this.getCartProductStorage());
  }

  /**
   * Добавить товара в корзину
   * @param id - идентификатор товара
   * @param count - колличетва товара
   */
  addProduct(id: number, count: number = 1): void {
    const cartProduct = this.cartProducts.value.find(cp => cp.productId === id);
    if (!cartProduct) {
      const newCartProducts = [...this.cartProducts.value, {productId: id, count: count}];
      this.cartProducts.next(newCartProducts);
      this.setCartProductStorage(newCartProducts);
    } else {
      const newCartProduct = {...cartProduct, count: cartProduct.count + count};
      const newCartProducts = this.cartProducts.value.map(cp => cp.productId === id ? newCartProduct : cp);
      this.cartProducts.next(newCartProducts);
      this.setCartProductStorage(newCartProducts);
    }
  }

  /**
   * Удалить товар из корзины
   * @param id - идентификатор товара
   */
  removeProduct(id: number): void {
    const cartProduct = this.cartProducts.value.find(cp => cp.productId === id);
    if (cartProduct) {
      const newCartProducts = this.cartProducts.value.filter(cp => cp.productId !== id);
      this.cartProducts.next(newCartProducts);
      this.setCartProductStorage(newCartProducts);
    }
  }

  /**
   * Очистить корзину
   */
  clear(): void {
    this.cartProducts.next([]);
    this.setCartProductStorage([]);
  }

  /**
   * Установить количество товара в корзине
   * @param id - идентификатор товара
   * @param count - колличетва товара
   */
  setCountProduct(id: number, count: number = 1): void {
    const cartProduct = this.cartProducts.value.find(cp => cp.productId === id);
    if (cartProduct) {
      const newCartProduct = {...cartProduct, count: count};
      const newCartProducts = this.cartProducts.value.map(cp => cp.productId === id ? newCartProduct : cp);
      this.cartProducts.next(newCartProducts);
      this.setCartProductStorage(newCartProducts);
    }
  }

  /**
   * Проверка наличие товара в корзине
   * @param id - идентификатор товара
   */
  hasProduct(id: number): Observable<boolean> {
    return this.cartProducts.asObservable()
      .pipe(
        map(cartProducts => !!cartProducts.find(cartProduct => cartProduct.productId === id)),
        shareReplay(1),
      );
  }

  /**
   * Получить список товаров в корзине
   */
  getList(): Observable<Array<CartProduct>> {
    return this.cartProducts.asObservable();
  }

  /**
   * Получить массив товаров в корзине из хранилища
   */
  private getCartProductStorage(): Array<CartProduct> {
    const changeLikeString = this.localStorage.getItem('cart-products');
    let cartProducts = [];
    if (changeLikeString) {
      cartProducts = JSON.parse(changeLikeString);
    }

    return cartProducts;
  }

  /**
   * Сохранить массив товаров в корзине в хранилище
   * @param cartProducts
   */
  private setCartProductStorage(cartProducts: Array<CartProduct>): void {
    this.localStorage.setItem('cart-products', JSON.stringify(cartProducts));
  }
}
