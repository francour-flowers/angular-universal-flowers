import { Category, Menu, MenuLink } from '@alexsmolin/shop-core';

/**
 * Основная информация сайта
 */
export interface MainData {
  /**
   * Название сайта
   */
  siteName: string;
  /**
   * Номер телефона
   */
  phoneNumber: MenuLink;
  /**
   * Список меню
   */
  menus: Array<Menu>;

  /**
   * Категории
   */
  categories: Array<Category>;
}
