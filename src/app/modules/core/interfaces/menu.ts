/**
 * Тип ссылки в меню
 */
export enum MenuType {
  /**
   * Страница
   */
  Page = 1,
  /**
   * Блог
   */
  Blog,
  /**
   * Продукт
   */
  Product,
  /**
   * Категория
   */
  Category,
  /**
   * Произвольная ссылка
   */
  CustomLink,
  /**
   * Затычка
   */
  Gag,
}

/**
 * Ссылка меню
 */
export interface MenuLink {
  /**
   * Идентификатор
   */
  id: number;
  /**
   * Название
   */
  label: string;
  /**
   * Ярлык
   */
  slug?: string;
  /**
   * Адресс, путь
   */
  url?: string;
  /**
   * Количество товаров
   */
  count?: number;
  /**
   * Тип
   */
  type: MenuType;
  /**
   * Иконка
   */
  icon?: string;
  /**
   * Список ссылок
   */
  children: Array<MenuLink>;
}

/**
 * Меню
 */
export interface Menu {

  /**
   * Идентификатор
   */
  id: number;
  /**
   * Название меню
   */
  name: string;
  /**
   * Список ссылок
   */
  links: Array<MenuLink>;
}
