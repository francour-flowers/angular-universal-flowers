import { Product, ProductService } from '@alexsmolin/shop-core';
import { HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express';
import { EMPTY, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

/**
 * Резолвер для загрузки продукта
 */
@Injectable()
export class ProductResolver implements Resolve<Product> {
  private readonly response: Response;

  constructor(private productService: ProductService,
              private router: Router,
              @Optional() @Inject(RESPONSE) response: any) {
    this.response = response;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> {
    const slug = route.paramMap.get('slug');

    return this.productService.getBySlug(slug)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (error.status === 404) {
            this.setNotFound();
          }
          return EMPTY;
        })
      );
  }

  private setNotFound(message: string = 'not found'): void {
    if (this.response) {
      this.response.statusCode = 404;
      this.response.statusMessage = message;
    }
  }

}
