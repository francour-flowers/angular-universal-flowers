import { Product, ProductImage, SHOP_IMAGE_URL, ShopImageUrlPipe } from '@alexsmolin/shop-core';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { getParentsCategories } from '@app/modules/core/helpers/common';
import { CanonicalService } from '@app/modules/core/services/canonical.service';
import { CartService } from '@app/modules/core/services/cart.service';
import { ProductLikeService } from '@app/modules/core/services/product-like.service';
import { AddToCartNotificationComponent } from '@app/modules/shared/components/add-to-cart-notification/add-to-cart-notification.component';
import { Breadcrumb } from '@app/modules/shared/components/breadcrumbs/breadcrumbs.component';
import { Observable, of } from 'rxjs';
import { filter, flatMap, map, shareReplay, tap } from 'rxjs/operators';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.scss'],
  providers: [
    CanonicalService,
  ],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductPageComponent implements OnInit, OnDestroy {

  product: Observable<Product>;

  count: number = 1;

  isLike: Observable<Boolean> = of(false);
  hasCart: Observable<Boolean> = of(false);

  breadcrumbs: Observable<Array<Breadcrumb>>;


  constructor(private route: ActivatedRoute,
              private productLikeService: ProductLikeService,
              private cartService: CartService,
              private canonicalService: CanonicalService,
              private metaTagService: Meta,
              private titleService: Title,
              @Inject(SHOP_IMAGE_URL) private imageURL: string,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.product = this.route.data
      .pipe(
        map(data => data['product']),
        tap((p: Product) => {
          if (p) {
            if (p.metaTags) {
              this.titleService.setTitle(p.metaTags.title || p.name);
              this.metaTagService.addTags([
                {name: 'description', content: p.metaTags.description},
                {name: 'keywords', content: p.metaTags.keywords},
                {name: 'og:image', content: this.getImageURL(p.productImages)},
              ]);
            } else {
              this.titleService.setTitle(p.name);
            }
            this.canonicalService.create();
          } else {
            this.titleService.setTitle('Такой страницы не существует');
            this.canonicalService.remove();
          }
        }),
        filter(Boolean),
        shareReplay(1),
      );

    this.isLike = this.product
      .pipe(
        flatMap(product => this.productLikeService.isLike(product.id)),
      );
    this.hasCart = this.product
      .pipe(
        flatMap(product => this.cartService.hasProduct(product.id)),
      );

    this.breadcrumbs = this.product
      .pipe(
        map(product => {
          const mainCategory = product.productCategories.find(pc => pc.main);
          let categories = [];

          if (mainCategory) {
            categories = getParentsCategories(mainCategory.categoryId, product.productCategories.map(pc => pc.category));
          }

          const breadcrumbs: Array<Breadcrumb> = categories.map(category => {
            const url = getParentsCategories(category.id, product.productCategories.map(pc => pc.category))
              .map(c => c.slug)
              .join('/');

            return {
              name: category.name,
              routerLink: `/catalog/${url}`
            };
          });

          breadcrumbs.push({
            name: product.name
          });

          return breadcrumbs;
        }),
      );
  }

  ngOnDestroy(): void {
    this.canonicalService.remove();

    this.titleService.setTitle('');

    this.metaTagService.removeTag('description');
    this.metaTagService.removeTag('keywords');
    this.metaTagService.removeTag('name="og:image"');
  }

  /**
   * Поменять стутус нравится
   */
  toggleLike(): void {
    this.product
      .pipe(
        map(product => product.id),
        flatMap(id => this.productLikeService.toggleLike(id))
      )
      .subscribe();
  }

  /**
   * Добавить в корзину
   */
  addInCart(): void {
    this.product
      .pipe(
        tap(product => {
          this.cartService.addProduct(product.id);

          this.matSnackBar.openFromComponent(AddToCartNotificationComponent, {
            duration: 5000,
            panelClass: ['min-vw-100', 'm-0', 'rounded-0', 'bg-light', 'text-dark'],
            verticalPosition: 'top',
            data: product,
          });
        })
      )
      .subscribe();
  }

  private getImageURL(productImages: Array<ProductImage>): string {
    const maimImage = productImages.find(pi => pi.main);
    if (maimImage) {
      const shopImageUrlPipe = new ShopImageUrlPipe(this.imageURL);
      return shopImageUrlPipe.transform(maimImage.image, 540, 340);
    }
    return null;
  }
}
