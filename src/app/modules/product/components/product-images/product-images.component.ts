import { ProductImage } from '@alexsmolin/shop-core';
import { isPlatformBrowser } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  NgZone,
  OnDestroy,
  Output,
  PLATFORM_ID
} from '@angular/core';
import { TINY_SLIDER } from '@app/local-storage';
import { TinySliderInstance } from 'tiny-slider';

@Component({
  selector: 'app-product-images',
  templateUrl: './product-images.component.html',
  styleUrls: ['./product-images.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductImagesComponent implements OnDestroy, AfterViewInit {
  /**
   * Идекс отображаемого изображения
   */
  displayIndex: number = 0;
  /**
   * Статус нравится
   */
  @Input() isLike: boolean;
  /**
   * Список изображений продукта
   */
  @Input() productImage: Array<ProductImage>;
  /**
   * Событие переключения статуса нравится
   */
  @Output() toggleLike: EventEmitter<void> = new EventEmitter();
  /**
   * Слайдер
   */
  private slider: TinySliderInstance;

  constructor(private cdr: ChangeDetectorRef,
              private ngZone: NgZone,
              private element: ElementRef,
              @Inject(TINY_SLIDER) private tns: any,
              @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnDestroy(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.slider.destroy();
    }
  }

  ngAfterViewInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.ngZone.runOutsideAngular(() => {
        const slider = this.tns.create({
          container: this.element.nativeElement.querySelector('.my-slider'),
          items: 1,
          touch: true,
          mouseDrag: true,
          controls: false,
          nav: false,
        });

        slider.events.on('indexChanged', (a: any) => {

          this.ngZone.run(() => {
            this.displayIndex = a.displayIndex - 1;
            this.cdr.markForCheck();
          });
        });

        this.ngZone.run(() => {
          this.slider = slider;
        });
      });
    }
  }

  showImage(number: number): void {
    this.slider.goTo(number);
  }

}
