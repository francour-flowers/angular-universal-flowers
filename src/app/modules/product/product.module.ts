import { ShopCoreModule } from '@alexsmolin/shop-core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ProductImagesComponent } from '@app/modules/product/components/product-images/product-images.component';
import { ProductPageComponent } from '@app/modules/product/components/product-page/product-page.component';
import { ProductResolver } from '@app/modules/product/resolvers/product.resolver';
import { SharedModule } from '@app/modules/shared/shared.module';

@NgModule({
  declarations: [
    ProductPageComponent,
    ProductImagesComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    ShopCoreModule,
    RouterModule.forChild([
      {
        path: ':slug',
        component: ProductPageComponent,
        pathMatch: 'full',
        resolve: {
          product: ProductResolver,
        },
      }
    ]),
  ],
  providers: [
    ProductResolver,
  ],
})
export class ProductModule {
}
