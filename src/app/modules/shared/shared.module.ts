import { ShopCoreModule } from '@alexsmolin/shop-core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatSidenavModule
} from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule } from '@angular/material/menu';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from '@app/modules/shared/components/page-not-found/page-not-found.component';
import { GetNameLinkPipe } from '@app/modules/shared/pipes/get-name-link.pipe';
import { AddToCartNotificationComponent } from './components/add-to-cart-notification/add-to-cart-notification.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { CounterComponent } from './components/counter/counter.component';
import { OrderDialogComponent } from './components/order-dialog/order-dialog.component';
import { PricePipe } from './components/price.pipe';
import { ProductCardComponent } from './components/product-card/product-card.component';
import { SliderComponent } from './components/slider/slider.component';
import { CategoryLinkPipe } from './pipes/category-link.pipe';
import { LinkHrefPipe } from './pipes/link-href.pipe';
import { SafePipe } from './pipes/safe.pipe';

@NgModule({
  declarations: [
    ProductCardComponent,
    PricePipe,
    CounterComponent,
    BreadcrumbsComponent,
    SafePipe,
    AddToCartNotificationComponent,
    OrderDialogComponent,
    CategoryLinkPipe,
    PageNotFoundComponent,
    LinkHrefPipe,
    SliderComponent,
    GetNameLinkPipe,
  ],
  entryComponents: [
    AddToCartNotificationComponent,
    OrderDialogComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatBadgeModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSidenavModule,
    MatSelectModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    RouterModule,
    MatFormFieldModule,
    ShopCoreModule,
    MatSnackBarModule,
    MatStepperModule,
    MatDialogModule,
    MatMenuModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
    MatBadgeModule,
    MatInputModule,
    MatAutocompleteModule,
    MatSidenavModule,
    MatSelectModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    ProductCardComponent,
    PricePipe,
    MatFormFieldModule,
    CounterComponent,
    BreadcrumbsComponent,
    SafePipe,
    MatSnackBarModule,
    MatStepperModule,
    CategoryLinkPipe,
    PageNotFoundComponent,
    LinkHrefPipe,
    MatMenuModule,
    ShopCoreModule,
    SliderComponent,
    GetNameLinkPipe,
  ],
})
export class SharedModule {
}
