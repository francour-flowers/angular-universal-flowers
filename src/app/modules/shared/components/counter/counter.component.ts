import { ChangeDetectorRef, Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CounterComponent),
      multi: true,
    },
  ]
})
export class CounterComponent implements ControlValueAccessor {

  count: number = 1;

  isDisabled: boolean = false;

  private onChange: Function = null;

  private onTouched: Function = null;

  constructor(private cdr: ChangeDetectorRef) {
  }

  get value(): number {
    return this.count;
  }

  set value(value: number) {
    this.count = value < 1 ? 1 : value;
    this.updateValue();
  }

  writeValue(value: number): void {
    this.count = value;
    this.cdr.markForCheck();
  }

  registerOnChange(fn: Function): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: Function): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  remove(): void {
    this.count = --this.count;
    this.updateValue();
  }

  add(): void {
    this.count = ++this.count;
    this.updateValue();
  }

  /**
   * Обновление значения ngModel
   */
  private updateValue(): void {
    if (this.onChange) {
      this.onChange(this.count);
    }
  }
}
