import { Image, Product } from '@alexsmolin/shop-core';
import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA, MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'app-add-to-cart-notification',
  templateUrl: './add-to-cart-notification.component.html',
  styleUrls: ['./add-to-cart-notification.component.scss']
})
export class AddToCartNotificationComponent {

  constructor(@Inject(MAT_SNACK_BAR_DATA) public product: Product,
              private snackRef: MatSnackBarRef<AddToCartNotificationComponent>) {
  }

  getMainImage(product: Product): Image {
    const productImage = product.productImages.find(productImg => productImg.main);
    if (productImage) {
      return productImage.image;
    }

    return null;
  }

  dismiss(): void {
    this.snackRef.dismiss();
  }
}
