import { Component, Input } from '@angular/core';

export interface Breadcrumb {
  name: string;
  routerLink?: string;
}

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent {
  @Input() breadcrumbs: Array<Breadcrumb>;
}
