import { Image, Product } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, Inject, Input, OnInit, PLATFORM_ID } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CartService } from '@app/modules/core/services/cart.service';
import { ProductLikeService } from '@app/modules/core/services/product-like.service';
import { AddToCartNotificationComponent } from '@app/modules/shared/components/add-to-cart-notification/add-to-cart-notification.component';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductCardComponent implements OnInit {
  @Input() product: Product;
  isLike: Observable<Boolean> = of(false);
  hasCart: Observable<Boolean> = of(false);

  constructor(private productLikeService: ProductLikeService,
              private cartService: CartService,
              private matSnackBar: MatSnackBar,
              @Inject(PLATFORM_ID) private platformId: any) {
  }


  ngOnInit(): void {
    this.isLike = this.productLikeService.isLike(this.product.id);
    this.hasCart = this.cartService.hasProduct(this.product.id);
  }

  toggleLike(): void {
    this.productLikeService.toggleLike(this.product.id).subscribe();
  }

  getMainImage(product: Product): Image {
    const productImage = product.productImages.find(productImg => productImg.main);
    if (productImage) {
      return productImage.image;
    }

    return null;
  }

  addInCart(): void {
    this.cartService.addProduct(this.product.id);

    this.matSnackBar.openFromComponent(AddToCartNotificationComponent, {
      duration: 5000,
      panelClass: ['min-vw-100', 'm-0', 'p-0', 'rounded-0', 'bg-light', 'text-dark'],
      verticalPosition: 'top',
      data: this.product,
    });
  }
}
