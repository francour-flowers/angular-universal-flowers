import { MenuLink } from '@alexsmolin/shop-core';
import { Pipe, PipeTransform } from '@angular/core';
import { getNameLink } from '@app/modules/core/helpers/common';

/**
 * Получить имя ссылки
 */
@Pipe({
  name: 'getNameLink'
})
export class GetNameLinkPipe implements PipeTransform {

  transform(link: MenuLink): any {
    return getNameLink(link);
  }

}
