import { Category, MenuLink, MenuType } from '@alexsmolin/shop-core';
import { Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getParentsCategories } from '@app/modules/core/helpers/common';

@Pipe({
  name: 'linkHref'
})
export class LinkHrefPipe implements PipeTransform {

  constructor(private route: ActivatedRoute) {
  }

  transform(menuLink: MenuLink): string {
    switch (menuLink.type) {
      case MenuType.Product:
        return `/product/${menuLink.product.slug}`;
      case MenuType.Category:
        const categories: Array<Category> = this.route.snapshot.data['mainData'] ? this.route.snapshot.data['mainData'].categories : [];
        const url = getParentsCategories(menuLink.categoryId, categories)
          .map(c => c.slug)
          .join('/');

        return `/catalog/${url}`;
      case MenuType.Page:
        return `/${menuLink.page.slug}`;
      case MenuType.CustomLink:
        return menuLink.url;
      default:
        return null;
    }
  }

}
