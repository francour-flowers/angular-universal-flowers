import { Category } from '@alexsmolin/shop-core';
import { Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { getParentsCategories } from '@app/modules/core/helpers/common';

@Pipe({
  name: 'categoryLink'
})
export class CategoryLinkPipe implements PipeTransform {
  constructor(private route: ActivatedRoute) {
  }

  transform(categoryID: number, args?: any): string {

    const categories: Array<Category> = this.route.snapshot.data['mainData'] ? this.route.snapshot.data['mainData'].categories : [];
    const url = getParentsCategories(categoryID, categories)
      .map(c => c.slug)
      .join('/');

    return `/catalog/${url}`;
  }

}
