import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CatalogRoutingModule } from '@app/modules/catalog/catalog-routing.module';
import { CatalogResolver } from '@app/modules/catalog/resolvers/catalog.resolver';
import { GridSelectorModule } from '@app/modules/grid-selector/grid-selector.module';
import { PaginatorModule } from '@app/modules/paginator/paginator.module';
import { SharedModule } from '@app/modules/shared/shared.module';
import { SortableModule } from '@app/modules/sortable/sortable.module';
import { CatalogComponent } from './components/category/catalog.component';

@NgModule({
  imports: [
    CommonModule,
    CatalogRoutingModule,
    SharedModule,
    PaginatorModule,
    SortableModule,
    GridSelectorModule,
  ],
  providers: [
    CatalogResolver,
  ],
  declarations: [
    CatalogComponent,
  ],
})
export class CatalogModule {
}
