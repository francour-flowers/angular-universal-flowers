import { Image, SHOP_IMAGE_URL, ShopImageUrlPipe } from '@alexsmolin/shop-core';
import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Catalog } from '@app/modules/catalog/interfaces/catalog.interface';
import { CanonicalService } from '@app/modules/core/services/canonical.service';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss'],
  providers: [
    CanonicalService,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CatalogComponent implements OnInit, OnDestroy {

  catalog: Observable<Catalog>;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private metaTagService: Meta,
              private titleService: Title,
              private canonicalService: CanonicalService,
              @Inject(SHOP_IMAGE_URL) private imageURL: string,
              @Inject(PLATFORM_ID) private platformId: Object) {
  }

  ngOnInit(): void {
    this.catalog = this.route.data
      .pipe(
        map(data => data['catalog']),
        tap((c: Catalog) => {
          if (c) {
            if (c.category.metaTags) {
              this.titleService.setTitle(c.category.metaTags.title || c.category.name);
              this.metaTagService.addTags([
                {name: 'description', content: c.category.metaTags.description},
                {name: 'keywords', content: c.category.metaTags.keywords},
                {name: 'og:image', content: this.getImageURL(c.category.image)},
              ]);
            } else {
              this.titleService.setTitle(c.category.name);
            }
            this.canonicalService.create();
          } else {
            this.titleService.setTitle('Такой страницы не существует');
            this.canonicalService.remove();
          }
        }),
        shareReplay(1),
      );
  }

  ngOnDestroy(): void {
    this.canonicalService.remove();

    this.titleService.setTitle('');

    this.metaTagService.removeTag('description');
    this.metaTagService.removeTag('keywords');
    this.metaTagService.removeTag('name="og:image"');
  }

  private getImageURL(image: Image): string {
    if (image) {
      const shopImageUrlPipe = new ShopImageUrlPipe(this.imageURL);
      return shopImageUrlPipe.transform(image, 540, 340);
    }
    return null;
  }
}
