import { CategoryService, ForOrder, ProductField, ProductService, QueryParams, Sort } from '@alexsmolin/shop-core';
import { Inject, Injectable, Optional } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Catalog } from '@app/modules/catalog/interfaces/catalog.interface';
import { getParentsCategories } from '@app/modules/core/helpers/common';
import { PaginatorService } from '@app/modules/paginator/paginator.service';
import { Breadcrumb } from '@app/modules/shared/components/breadcrumbs/breadcrumbs.component';
import { RESPONSE } from '@nguniversal/express-engine/tokens';
import { Response } from 'express';
import { Observable } from 'rxjs';
import { defaultIfEmpty, filter, first, flatMap, map, switchMap, tap } from 'rxjs/operators';

/**
 * Сортировка по умолчанию
 */
const DEFAULT_SORT = {
  field: ProductField.CREATED_AT,
  order: ForOrder.DESC
};

const LIMIT = 24;

/**
 * Резолвер для загрузки каталога
 */
@Injectable()
export class CatalogResolver implements Resolve<Catalog> {
  private readonly response: Response;

  constructor(private productService: ProductService,
              private router: Router,
              private categoryService: CategoryService,
              private paginatorService: PaginatorService,
              @Optional() @Inject(RESPONSE) response: any) {
    this.response = response;
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Catalog> {
    let page = Number(route.queryParams['page']);
    page = page > 0 ? page : 1;

    return this.categoryService.getTree()
      .pipe(
        first(),
        map((categories) => {
          const segmentsCategories = route.url.map(s => ({segment: s.path, category: null}));
          for (let i = 0; i < segmentsCategories.length; i++) {
            let category;

            if (i === 0) {
              category = categories.find(c => c.slug === segmentsCategories[i].segment);
            } else {
              category = (segmentsCategories[i - 1].category.children || []).find(c => c.slug === segmentsCategories[i].segment);
            }

            if (category) {
              segmentsCategories[i].category = category;
            } else {
              return null;
            }
          }
          return segmentsCategories.map(sg => sg.category);
        }),
        filter(Boolean),
        flatMap(categories => this.categoryService.getById(categories[categories.length - 1].id)
          .pipe(
            map(category => ([category, categories])),
          )),
        map(([category, categories]) => {
          const breadcrumbs: Array<Breadcrumb> = categories.map(c => {
            const url = getParentsCategories(c.id, categories)
              .map(pc => pc.slug)
              .join('/');

            return {
              name: c.name,
              routerLink: category === c ? null : `/catalog/${url}`,
            };
          });

          return ([category, breadcrumbs]);
        }),
        switchMap(([category, breadcrumbs]) => {
          const queryParams: QueryParams<ProductField> = {
            offset: (page - 1) * LIMIT,
            limit: LIMIT,
            sort: [
              this.convertSortStrToSort(route.queryParams['sort'])
            ],
            filter: [
              {
                field: ProductField.CATEGORY_ID,
                value: String(category.id),
              }
            ]
          };
          return this.productService.getList(queryParams)
            .pipe(
              first(),
              tap(list => {
                this.paginatorService.updatePaginator(page, Math.ceil(list.totalCount / LIMIT));
              }),
              map(list => ({
                productList: list,
                category: category,
                breadcrumbs: breadcrumbs,
              })),
            );
        }),
        defaultIfEmpty(null),
        tap(category => {
          if (!category) {
            this.setNotFound();
          }
        }),
      );
  }

  private convertSortStrToSort(sort: string): Sort<ProductField> {
    const order = sort && sort.charAt(0) === '-' ? ForOrder.DESC : ForOrder.ASC;
    const field = sort && order === ForOrder.DESC ? sort.substr(1) : sort;
    if ((<any>Object).values(ProductField).includes(field)) {
      return {
        order: order,
        field: field as ProductField,
      };
    }

    return DEFAULT_SORT;
  }

  private setNotFound(message: string = 'not found'): void {
    if (this.response) {
      this.response.statusCode = 404;
      this.response.statusMessage = message;
    }
  }
}
