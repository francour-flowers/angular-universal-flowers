import { Category, List, Product } from '@alexsmolin/shop-core';
import { Breadcrumb } from '@app/modules/shared/components/breadcrumbs/breadcrumbs.component';

/**
 * Каталог
 */
export interface Catalog {
  /**
   * Категория
   */
  category: Category;
  /**
   * Хлебные крошки
   */
  breadcrumbs: Array<Breadcrumb>;
  /**
   * Список продуктов
   */
  productList: List<Product>;
}
