import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogComponent } from '@app/modules/catalog/components/category/catalog.component';
import { CatalogResolver } from '@app/modules/catalog/resolvers/catalog.resolver';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '**',
        runGuardsAndResolvers: 'always',
        resolve: {
          // mainData: GlobalDataResolver,
          catalog: CatalogResolver
        },
        component: CatalogComponent,
      }
    ]
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule {
}
