import { NgModule } from '@angular/core';
import { BrowserTransferStateModule } from '@angular/platform-browser';
import { AppComponent } from '@app/app.component';
import { AppModule } from '@app/app.module';
import { LOCALSTORAGE, TINY_SLIDER } from '@app/local-storage';
import { StateTransferInitializerModule } from '@nguniversal/common';
import { TinySliderInstance, TinySliderSettings, tns } from 'tiny-slider/src/tiny-slider';

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    AppModule,
    StateTransferInitializerModule,
    BrowserTransferStateModule,
  ],
  providers: [
    {
      provide: LOCALSTORAGE,
      useValue: window.localStorage
    },
    {
      provide: TINY_SLIDER,
      useValue: {
        create(options: TinySliderSettings): TinySliderInstance {
          return tns(options);
        }
      }
    }
  ],
})
export class AppBrowserModule {
}
