import { MenusResolver, SHOP_API_URL, SHOP_ICON_URL, SHOP_IMAGE_URL, ShopCoreModule } from '@alexsmolin/shop-core';
import { isPlatformBrowser } from '@angular/common';
import { APP_ID, Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '@env/environment';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { MetaLoader, MetaModule, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CoreModule } from './modules/core/core.module';
import { SharedModule } from './modules/shared/shared.module';

export function metaFactory(): MetaLoader {
  return new MetaStaticLoader({
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: ' | ',
    applicationName: 'App applicationName',
    defaults: {
      'title': 'Default page title',
      'description': 'Default description',
      'og:image': '',
      'og:site_name': 'App site Universal',
      'og:type': 'website',
      'og:locale': 'ru_RU',
    },
  });
}

@NgModule({
  imports: [
    BrowserModule.withServerTransition({appId: 'my-app'}),
    TransferHttpCacheModule,
    BrowserAnimationsModule,
    CoreModule.forRoot(),
    SharedModule,
    ShopCoreModule,
    AppRoutingModule,
    MetaModule.forRoot({
      provide: MetaLoader,
      useFactory: metaFactory,
    }),
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    {
      provide: SHOP_API_URL,
      useValue: environment.apiUrl
    },
    {
      provide: SHOP_IMAGE_URL,
      useValue: environment.imgUrl
    },
    {
      provide: SHOP_ICON_URL,
      useValue: environment.iconUrl
    },
    MenusResolver,
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    // tslint:disable-next-line
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
